/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.lignacio.gateway.services;

import ar.com.lignacio.gateway.dto.ServiceDto;
import java.util.List;

/**
 *
 * @author unknown
 */
public interface HomeService {
    List<ServiceDto> getAllServices();
    /**
     * Devuelve todos los servicios indicando a cuales tiene permiso el usuario
     * @return 
     */
    List<ServiceDto> getUserServices();
    /**
     * Servicio mas estricto, solo devuelve servicios del usuario,
     * no expone los servicios sin permisos
     * @return ServiceDto[]
     */
    List<ServiceDto> getUserServicesOnly();
 
    
}
