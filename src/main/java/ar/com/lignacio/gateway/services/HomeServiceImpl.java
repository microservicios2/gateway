/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.lignacio.gateway.services;

import ar.com.lignacio.gateway.dto.ServiceDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;

/**
 *
 * @author unknown
 */
@Service
public class HomeServiceImpl implements HomeService {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Override
    public List<ServiceDto> getAllServices() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ServiceDto> getUserServices() {
        List<ServiceDto> servicios = new ArrayList<ServiceDto>();

        this.discoveryClient.getServices().forEach(serviceName -> {
            servicios.add(new ServiceDto(serviceName, true, true));
        });
  
        return servicios;
    }

    @Override
    public List<ServiceDto> getUserServicesOnly() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
