/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.lignacio.gateway.controllers;

import ar.com.lignacio.gateway.services.HomeService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author unknown
 */
@Controller
public class HomeController {

    @Autowired
    private HomeService homeService;

    @GetMapping("/")
    public String index() {
        
        return "index";
    }
    
    @GetMapping("/home")
    public String home_index(Authentication authentication,Model model) {
        model.addAttribute("username",authentication.getName());
        model.addAttribute("services", homeService.getUserServices());
        return "home/index";
    }
    
    @GetMapping("/logout")
    public String logout(HttpServletRequest request) throws ServletException {
    request.logout();
    return "index";
    }
}
