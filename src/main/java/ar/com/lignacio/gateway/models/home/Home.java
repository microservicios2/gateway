package ar.com.lignacio.gateway.models.home;

import ar.com.lignacio.gateway.dto.ServiceDto;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author unknown
 */
public class Home {
    List<ServiceDto> services;

    public Home() {
    }

    public List<ServiceDto> getServices() {
        return services;
    }

    public void setServices(List<ServiceDto> services) {
        this.services = services;
    }
    
    
}

