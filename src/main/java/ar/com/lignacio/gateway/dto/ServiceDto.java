/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.lignacio.gateway.dto;

/**
 *
 * @author unknown
 */
public class ServiceDto {
    
    private String serviceName;
    private String serviceDescription;
    private String serviceUrl;
    private Boolean serviceAuthorized;
    private Boolean serviceEnabled;

    public ServiceDto(String nombre,Boolean tienePermisos,Boolean visible){
        this.serviceName = nombre;
        this.serviceAuthorized = tienePermisos;
        this.serviceEnabled = visible;
        
    }
    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public Boolean getServiceAuthorized() {
        return serviceAuthorized;
    }

    public void setServiceAuthorized(Boolean serviceAuthorized) {
        this.serviceAuthorized = serviceAuthorized;
    }

    public Boolean getServiceEnabled() {
        return serviceEnabled;
    }

    public void setServiceEnabled(Boolean serviceEnabled) {
        this.serviceEnabled = serviceEnabled;
    }
    
    
    
}
